/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.sasipa.lab2;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Lab2 {

    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (IsWin()) {
                printTable();
                printWin();
                break;
            }
            if (IsDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
        inputContinuous();
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO program!!!");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " turn ");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
            if (IsDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
        inputContinuous();
    }

    private static boolean IsWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;

    }

    private static boolean checkX2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("Congratulations! Player " + currentPlayer + " Win!");
    }

    private static boolean IsDraw() {

        if (checkDraw()) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
             }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("The result is Draw!!!!!!");
    }

    private static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static void inputContinuous() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Do you want to play again? (y/n): ");
            String newGame = kb.next();
            if (newGame.equalsIgnoreCase("y")) {
                resetGame();
                main(null);
                break;
            } else if (newGame.equalsIgnoreCase("n")) {
                System.out.println("Thank you for playing! ByeBye!!!");
                break;
            } else {
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
            }
        }
    }

    private static void resetGame() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'X';
    }
}
